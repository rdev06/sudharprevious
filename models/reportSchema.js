const mongoose = require('mongoose');

//for defining schema
let reportSchema = mongoose.Schema({
  organisationId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'organisation',
    required: true
  },
  ppId: { type: mongoose.Schema.Types.ObjectId, ref: 'pp', required: true },
  machineId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'machine',
    required: true
  },
  tgId: { type: String, required: true },
  status: {
    type: String,
    enum: ['started', 'stopped', 'idle', 'pause', 'disconnect'],
    required: true
  },
  ictpu: { type: Number, required: true },
  oee: {
    availability: { type: Number, default: 0 },
    quality: { type: Number, default: 0 },
    performance: { type: Number, default: 0 },
    oeePercent: { type: Number, default: 0 }
  },
  cycleTime: { type: Number, default: 0 },
  nC: { type: Number, default: 0, required: true }, //total number of count default to 0
  bC: { type: Number, default: 0, required: true }, //total number of bad count default to 0
  downTime: { type: Number, default: 0, required: true },
  MTBF: { type: Number, default: 0 },
  MTTR: { type: Number, default: 0 },
  UpTime: { type: Number, default: 0 },
  quantity: { type: Number, default: 0 },
  workers: [
    {
      wemail: { type: String },
      wphone: { type: Number },
      time: { type: Date }
    }
  ],
  planedDownTime: [
    {
      from: { type: Date },
      to: { type: Date },
      cause: { type: String }
    }
  ],
  unPlanedDownTime: [
    {
      from: { type: Date },
      to: { type: Date },
      cause: { type: String },
      comment: { type: String }
    }
  ],
  bCdetails: { type: mongoose.Schema.Types.Mixed }
});

//for defining models
module.exports = mongoose.model('report', reportSchema);
