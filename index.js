const mongoose = require('mongoose');
mongoose.connect('mongodb://roshan:ostindev06@ds137508.mlab.com:37508/oeebk', {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
  useUnifiedTopology: true
});
const db = mongoose.connection;
const reportSchema = require('./models/reportSchema');
db.once('open', () =>
  reportSchema
    .updateMany(
      { tgId: 'TG0004', ppId: '' },
      {
        $set: { ppId: '5e3802150cd6c42a959221ce' }
      },
      { new: true }
    )
    .exec()
    .then(report => console.log(report))
    .catch(err => console.log(err))
);
